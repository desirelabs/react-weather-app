import * as React from 'react'
// Reactstrap
import Row from 'reactstrap/lib/Row';
import Button from 'reactstrap/lib/Button';
import Col from 'reactstrap/lib/Col';
import Input from 'reactstrap/lib/Input';
// Redux
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import { IHistoryEntry } from '../../store/history/history.interfaces';

import { css } from 'emotion';
import { setForecast } from '../../store/forecast/forecast.actions';
import { addHistoryEntry } from '../../store/history/history.actions';
import { Alert } from 'reactstrap';

interface IProps {
  history: {
    push: (route) => void
  }
  lastEntry: string
  historyList: Array<IHistoryEntry>
  addHistoryEntry: (value: string) => void
  setForecast: (value: string) => void
}

interface IState {
  message: string
}

const API_URL = 'https://api.openweathermap.org/data/2.5/forecast?lang=fr&units=metric&appid=29e52c75a233178c21cc073f64f7afbe'

class Search extends React.Component<IProps, IState> {
  constructor(props) {
    super(props)
    this.state = {
      message: ""
    }
    this.inputRef = React.createRef()
  }

  inputRef = null

  handleSubmit = (e) => {
    e.preventDefault()
    const city = this.inputRef.current.value.replace(/\s+/g, '+')
    fetch(`${API_URL}&q=${city}`).then(response => {
      response.json().then(data => {
        if (data.cod === '200') {
          this.props.setForecast(data)
          this.props.addHistoryEntry(this.inputRef.current.value)
          this.inputRef.current.value = ""
          this.props.history.push('/forecast')
        } else {
          this.setState({
            message: "Ville introuvable"
          })
        }
      })
    })
  }

  resetMessage = () => {
    !!this.state.message && this.setState({ message: '' })
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit} className={searchForm}>
        <Row>
          <Col>
          {!!this.state.message && 
            <Alert color="danger">{this.state.message}</Alert>
          }
          </Col>
        </Row>
        <Row>
          <Col xs={9}><Input type="text" innerRef={this.inputRef} defaultValue={this.props.lastEntry} onChange={this.resetMessage} /></Col>
          <Col><Button type="submit" color="success" style={{width: '100%'}}>-></Button></Col>
        </Row>
      </form>
    )
  }
}

const searchForm = css({
  height: '100vh',
  display: 'flex',
  flexFlow: 'column nowrap',
  justifyContent: 'center',
  padding: '0 15px'
})

const mapStateToProps = state => {
  return {
    historyList: state.history,
    search: state.search,
    lastEntry: state.history.length > 0 ? state.history[state.history.length - 1].entry : undefined
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setForecast: bindActionCreators(setForecast, dispatch),
    addHistoryEntry: bindActionCreators(addHistoryEntry, dispatch),
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search)