import * as React from 'react'
import {WeatherItem} from './WeatherItem'
import { shallow, configure } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16'

configure({adapter: new Adapter()})

describe('Weather Item component', () => {
  it('Should match snapshot', () => {
    const weatherItem = {}
    const wrapper = shallow(<WeatherItem value={weatherItem} />)
    expect(wrapper).toMatchSnapshot()
  })
})