import * as React from 'react'
import { css } from 'emotion';

const Loader = ({loading}: {loading: boolean}) => loading && (
  <div className={loader}>
    <img src={require('../assets/spinner.svg')} width="200" height="200" alt="Loading..." style={{margin: 'auto'}} />
  </div>
)

const loader = css({
  display: 'flex',
  position: 'absolute',
  left: 0,
  right: 0,
  top: 0,
  bottom: 0,
  backgroundColor: 'rgba(255, 255, 255, 0.65)'
})

export default Loader