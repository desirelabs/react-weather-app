import {SET_FORECAST} from './forecast.types'

export const setForecast = forecast => {
  return {
    type: SET_FORECAST,
    forecast
  }
}
