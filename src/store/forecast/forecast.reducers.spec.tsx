import {forecast} from './forecast.reducers'

describe('forecast reducer', () => {
  it('Should return an updated forecast', () => {
    const state = {}
    const action = {type: 'SET_FORECAST', forecast: {someKey: 'some val'}}
    const expected = {someKey: 'some val'}

    expect(forecast(state, action)).toEqual(expected)
  })
})