export interface IHistoryEntry {
  entry: string
  id: string
}