import {history} from './history.reducers'

describe('history reducer', () => {
  it('Should return an updated history', () => {
    const state = []
    const action = {type: 'INIT_HISTORY', history: [{entry: 'some val', id: 'some val'}]}
    const expected = [{entry: 'some val', id: 'some val'}]

    expect(history(state, action)).toEqual(expected)
  })
})