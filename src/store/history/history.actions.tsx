import { INIT_HISTORY, ADD_TO_HISTORY } from "./history.types";
import db from "../../config/indexdb";
import uuidv4 from 'uuid/v4'

import {IHistoryEntry} from './history.interfaces'

export const addHistoryEntry = (entry: string) => {
  return dispatch => {
    db.history.put({entry, id: uuidv4()}).then(() => {
      dispatch({
        type: ADD_TO_HISTORY,
        entry
      })
    }).catch(function(error) {
      alert("Ooops: " + error);
    })
  }
}

export const fetchHistory = () => {
  return dispatch => {
    let history: Array<IHistoryEntry> = []
    db.history.each(element => history.push(element))
    return dispatch({
      type: INIT_HISTORY,
      history
    })
  }
}