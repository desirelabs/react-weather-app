import Dexie from 'dexie'

const db: Dexie = new Dexie("history")

db.version(1).stores({
  history: 'entry,uid'
})

export default db